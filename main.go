package main

import (
	"bufio"
	"cmdiscord/terminal"
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
)

const (
	sigReady int = iota
)

var (
	signal        = make(chan int, 16)
	input         = make(chan rune, 16)
	session       *discordgo.Session
	width, height int
)

/*
[-----------------------------SERVERS-------------------]
[Channels][-----------Messages------------][---Members--]
[Channels][-----------Messages------------][---Members--]
[Channels][-----------Messages------------][---Members--]
[Channels][-----------Messages------------][---Members--]
[Channels][-----------Messages------------][---Members--]
[Channels][-----------Messages------------][---Members--]
[--Status--][-----------------Input---------------------]
*/

func main() {
	terminal.SetupTerminal()
	defer terminal.ResetTerminal()
	width, height, _ = terminal.GetSize()

	terminal.ClearTerminal()

	go echo()

	email := os.Getenv("DISCORD_EMAIL")
	pass := os.Getenv("DISCORD_PASSWORD")
	//token := os.Getenv("DISCORD_TOKEN")
	var err error
	session, err = discordgo.New(email, pass)
	if err != nil {
		fmt.Printf("Error creating discord session: %v", err)
		os.Exit(1)
	}

	session.AddHandler(ready)

	err = session.Open()
	if err != nil {
		fmt.Printf("Error opening discord websocket: %v", err)
		os.Exit(1)
	}

	fmt.Println("Logging in...")

	for sig := range signal {
		switch sig {
		case sigReady:
			terminal.ClearTerminal()
			renderServers()
			// renderChannels
			// renderMessages
			// renderUsers
		}
	}

	fmt.Println("Terminating...")

	session.Close()
}

func echo() {
	sc := bufio.NewReader(os.Stdin)

	for {
		l, _, _ := sc.ReadRune()

		if int(l) == 3 {
			close(signal)
		} else {
			input <- l
		}
	}
}
