package main

import (
	"cmdiscord/terminal"
	"fmt"
	"math"
)

func renderServers() {
	terminal.MoveCursor(0, 0)

	out := ""
	l := len(session.State.Guilds)
	for i, g := range session.State.Guilds {
		out += fmt.Sprintf(" %s ", g.Name)
		if i < l {
			out += fmt.Sprint("|")
		}
	}
	out = out[:int(math.Min(float64(len(out)), float64(width)))]
	fmt.Print(out)
}
