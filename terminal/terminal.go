package terminal

import (
	"fmt"
	"syscall"
	"os"
	"golang.org/x/crypto/ssh/terminal"
)

var term_in, term_out int
var oldState *terminal.State
func SetupTerminal() {
	term_in = int(syscall.Stdin)
	term_out = int(syscall.Stdout)

	var err error
	oldState, err = terminal.MakeRaw(term_in)
	if err != nil {
		fmt.Printf("Error enabling raw mode!\n%v\n", err)
		os.Exit(1)
	}
}

func ResetTerminal() {
	terminal.Restore(term_in, oldState)
}

func MoveCursor(x int, y int) {
    fmt.Printf("\033[%d;%dH", x, y)
}
func ClearTerminal() {
    fmt.Print("\033[2J")
}
func GetSize() (x, y int, err error) {
	return terminal.GetSize(term_out)
}
